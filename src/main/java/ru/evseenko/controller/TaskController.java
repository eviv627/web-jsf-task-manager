package ru.evseenko.controller;

import com.ocpsoft.pretty.faces.annotation.URLMapping;
import com.ocpsoft.pretty.faces.annotation.URLMappings;
import org.jetbrains.annotations.NotNull;
import org.primefaces.event.RowEditEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.web.context.WebApplicationContext;
import ru.evseenko.api.service.IProjectService;
import ru.evseenko.api.service.ITaskService;
import ru.evseenko.entity.Status;
import ru.evseenko.entity.dto.ProjectDTO;
import ru.evseenko.entity.dto.TaskDTO;
import ru.evseenko.entity.dto.UserDTO;
import ru.evseenko.exception.ServiceException;
import ru.evseenko.util.ControllerUtil;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Named("task")
@Scope(WebApplicationContext.SCOPE_REQUEST)
@URLMappings( mappings = {
        @URLMapping(id = "task",
                pattern = "/task",
                viewId = "/WEB-INF/views/task/list.xhtml"),
        @URLMapping(id = "taskAdd",
                pattern = "/task/add",
                viewId = "/WEB-INF/views/task/add.xhtml")
})
public class TaskController {

    private final ITaskService taskService;
    private final IProjectService projectService;
    private final UserDTO user;
    private final TaskDTO task;
    private List<TaskDTO> tasks;

    @Autowired
    public TaskController(ITaskService taskService, IProjectService projectService) {
        this.taskService = taskService;
        this.projectService = projectService;
        this.user = ControllerUtil.getCurrentUser();
        this.task = new TaskDTO();
    }

    @PostConstruct
    public void init() {
        tasks = taskService.getAll(user.getId());
    }

    public String addTask() {
        final String userId = user.getId();
        task.setUserId(userId);
        task.setId(UUID.randomUUID().toString());
        task.setCreateDate(new Date());
        taskService.persist(userId, task);

        @NotNull final FacesMessage msg = new FacesMessage("Task", "Task add success");
        FacesContext.getCurrentInstance().addMessage(null, msg);

        return "list.xhtml?faces-redirect=true";
    }

    public void onRowEdit(RowEditEvent event) {
        @NotNull final String userId = user.getId();
        @NotNull final Object obj = event.getObject();
        @NotNull final TaskDTO taskUpd;
        if (obj instanceof TaskDTO) {
            taskUpd = (TaskDTO) obj;
        } else {
            throw new ServiceException("invalid object");
        }

        taskService.update(userId, taskUpd);
        tasks = taskService.getAll(userId);

        @NotNull final FacesMessage msg = new FacesMessage("Task Edited", taskUpd.getName());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public void onRowCancel(RowEditEvent event) {
        final FacesMessage msg = new FacesMessage("Edit Cancelled", ((TaskDTO) event.getObject()).getName());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public String goToAdd() {
        return "add.xhtml?faces-redirect=true";
    }

    public void deleteTask(final TaskDTO task) {
        tasks.remove(task);
        taskService.delete(user.getId(), task);
        @NotNull final FacesMessage msg = new FacesMessage("Task Deleted", task.getName());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public List<TaskDTO> getTasks() {
        return tasks;
    }

    public List<ProjectDTO> getProjects() {
        return projectService.getAll(user.getId());
    }
    public List<Status> getStatuses() { return Arrays.asList(Status.values()); }


    public void setName(String name) {
        task.setName(name);
    }

    public String getName() {
        return task.getName();
    }

    public void setDescription(String description) {
        task.setDescription(description);
    }

    public String getDescription() {
        return task.getDescription();
    }

    public void setStartDate(Date startDate) {
        task.setStartDate(startDate);
    }

    public Date getStartDate() {
        return task.getStartDate();
    }

    public void setEndDate(Date endDate) {
        task.setEndDate(endDate);
    }

    public Date getEndDate() {
        return task.getEndDate();
    }

    public void setProjectId(String projectId) {
        task.setProjectId(projectId);
    }

    public String getProjectId() {
        return task.getProjectId();
    }

    public void setStatus(Status status) {
        task.setStatus(status);
    }

    public Status getStatus() {
        return task.getStatus();
    }
}
