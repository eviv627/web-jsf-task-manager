package ru.evseenko.controller;

import com.ocpsoft.pretty.faces.annotation.URLMapping;
import com.ocpsoft.pretty.faces.annotation.URLMappings;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.context.WebApplicationContext;
import ru.evseenko.api.service.IUserService;
import ru.evseenko.entity.Role;
import ru.evseenko.entity.dto.UserDTO;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import java.util.UUID;

@Named("registration")
@Scope(WebApplicationContext.SCOPE_REQUEST)
@URLMappings( mappings = {
        @URLMapping(id = "registration",
                pattern = "/registration",
                viewId = "/WEB-INF/views/user/registration.xhtml"),
        @URLMapping(id = "/login",
                pattern = "/login",
                viewId = "/WEB-INF/views/user/login.xhtml")
})
public class RegistrationController {

    private final IUserService userService;
    private final PasswordEncoder passwordEncoder;

    private final UserDTO user = new UserDTO();

    @Autowired
    public RegistrationController(IUserService userService, PasswordEncoder passwordEncoder) {
        this.userService = userService;
        this.passwordEncoder = passwordEncoder;
    }

    public void setLogin(String login) {
        user.setLogin(login);
    }

    public String getLogin() {
        return user.getLogin();
    }

    public void setPassword(String password) {
        user.setPassword(password);
    }

    public String getPassword() {
        return user.getPassword();
    }

    public void setPasswordConfirm(String passwordConfirm) {
        user.setPasswordConfirm(passwordConfirm);
    }

    public String getPasswordConfirm() {
        return user.getPasswordConfirm();
    }

    public String registration() {
        if(getLogin().length() < 5 || getLogin().length() > 16) {
            FacesContext.getCurrentInstance().addMessage("registerForm:login",
                    new FacesMessage("incorrect length", "incorrect length"));
            return "registration.xhtml";
        }

        if(getPassword().length() < 5 || getPassword().length() > 20) {
            FacesContext.getCurrentInstance().addMessage("registerForm:password",
                    new FacesMessage("incorrect length", "incorrect length"));
            return "registration.xhtml";
        }

        @Nullable final UserDTO isExists = userService.getUserByLogin(user.getLogin());

        if(isExists != null) {
            FacesContext.getCurrentInstance().addMessage("registerForm:login",
                    new FacesMessage("Already used", "Already used"));
            return "registration.xhtml";
        }

        if (!user.getPasswordConfirm().equals(user.getPassword())) {
            FacesContext.getCurrentInstance().addMessage("registerForm:passwordConfirm",
                    new FacesMessage("PASSWORDS DONT MATCH", "PASSWORDS DONT MATCH"));
            return "registration.xhtml";
        }

        user.setPasswordHash(passwordEncoder.encode(user.getPassword()));
        user.setId(UUID.randomUUID().toString());
        user.setRole(Role.ADMIN);
        user.setActive(true);
        userService.persist(user);

        FacesContext.getCurrentInstance().addMessage("loginForm",
                new FacesMessage("registration success", "registration success"));

        return "login.xhtml?faces-redirect=true";
    }
}
