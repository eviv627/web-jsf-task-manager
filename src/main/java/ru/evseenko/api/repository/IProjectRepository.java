package ru.evseenko.api.repository;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.evseenko.entity.Project;

import java.util.List;

@Repository
public interface IProjectRepository extends JpaRepository<Project, String>, IDomainRepository<Project> {

    @NotNull List<Project> findAll(@NotNull @Param("userId") String userId);

    @NotNull List<Project> findByNamePart(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("name") String name
    );

    @NotNull List<Project> findByDescriptionPart(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("description") String description
    );
}
