package ru.evseenko.api.entity;

public interface Identifiable {
    String getId();

    void setId(String id);
}
