package ru.evseenko.entity.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.evseenko.api.entity.DomainDTO;
import ru.evseenko.entity.Status;
import ru.evseenko.util.DateFormatUtil;

import javax.xml.bind.annotation.XmlSeeAlso;
import java.io.Serializable;
import java.util.Date;

@Setter
@Getter
@XmlSeeAlso({Status.class})
public class ProjectDTO implements Serializable, DomainDTO {

    public ProjectDTO() {
        id = "";
        name = "";
        description = "";
        startDate = new Date();
        endDate = new Date();
        createDate = new Date();
        status = Status.PLANNED;
        userId = "";
    }

    @NotNull
    private String id;

    @NotNull
    private String name;

    @NotNull
    private String description;

    @NotNull
    private Date startDate;

    @NotNull
    private Date endDate;

    @NotNull
    private Date createDate;

    @NotNull
    private Status status;

    @NotNull
    private String userId;

    @Override
    public String toString() {
        return "ProjectDTO {" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", startDate=" + DateFormatUtil.parseIsoDate(startDate) +
                ", endDate=" + DateFormatUtil.parseIsoDate(endDate) +
                ", createDate=" + DateFormatUtil.parseIsoDate(createDate) +
                ", status=" + status +
                ", userId='" + userId + '\'' +
                '}';
    }
}
