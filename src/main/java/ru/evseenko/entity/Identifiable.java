package ru.evseenko.entity;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

@Getter
@Setter
@MappedSuperclass
public abstract class Identifiable {

    @Id
    @Nullable
    @Column(name = "id", length = 36)
    private String id;

}
