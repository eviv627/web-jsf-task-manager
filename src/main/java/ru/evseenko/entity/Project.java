package ru.evseenko.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.Nullable;
import org.springframework.cache.annotation.Cacheable;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import java.util.List;

@Entity
@Setter
@Getter
@Cacheable
@NoArgsConstructor
@Table(name = "app_project")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@NamedQueries({
        @NamedQuery(
                name = Project.FIND_ALL,
                query = "select project from Project project where project.user.id = :userId",
                hints = { @QueryHint(name = "org.hibernate.cacheable", value = "true") }
                ),
        @NamedQuery(
                name = Project.FIND_BY_NAME,
                query = "select project from Project project where project.name like CONCAT(:name,'%') " +
                "and project.user.id = :userId",
                hints = { @QueryHint(name = "org.hibernate.cacheable", value = "true") }
                ),
        @NamedQuery(
                name = Project.FIND_BY_DESC,
                query = "select project from Project project where project.description like CONCAT(:description,'%') " +
                        "and project.user.id = :userId",
                hints = { @QueryHint(name = "org.hibernate.cacheable", value = "true") }
                )
})
public class Project extends Domain {

    public static final String DELETE_ALL = "Project.deleteAll";
    public static final String FIND_ALL = "Project.findAll";
    public static final String FIND_BY_NAME = "Project.findByNamePart";
    public static final String FIND_BY_DESC = "Project.findByDescriptionPart";

    @Nullable
    @OneToMany(mappedBy = "project", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<Task> tasks;
}
