package ru.evseenko.util;

import ru.evseenko.api.entity.DomainDTO;
import ru.evseenko.entity.Domain;

public class DomainEntityMapperUtil {

    public static void setUpDomainDTO(Domain entity, DomainDTO entityDTO) {
        entityDTO.setId(entity.getId());
        entityDTO.setName(entity.getName());
        entityDTO.setDescription(entity.getDescription());
        entityDTO.setStartDate(entity.getStartDate());
        entityDTO.setEndDate(entity.getEndDate());
        entityDTO.setCreateDate(entity.getCreateDate());
        entityDTO.setStatus(entity.getStatus());
    }

    public static void setUpDomainEntity(Domain entity, DomainDTO entityDTO) {
        entity.setId(entityDTO.getId());
        entity.setName(entityDTO.getName());
        entity.setDescription(entityDTO.getDescription());
        entity.setStartDate(entityDTO.getStartDate());
        entity.setEndDate(entityDTO.getEndDate());
        entity.setCreateDate(entityDTO.getCreateDate());
        entity.setStatus(entityDTO.getStatus());
    }
}
