package ru.evseenko.api.entity;

import ru.evseenko.entity.Status;

import java.util.Date;

public interface DomainDTO extends Identifiable, SortableEntity {

    String getName();

    void setName(String name);

    String getDescription();

    void setDescription(String description);

    Date getStartDate();

    void setStartDate(Date startDate);

    Date getEndDate();

    void setEndDate(Date endDate);

    Date getCreateDate();

    void setCreateDate(Date createDate);

    Status getStatus();

    void setStatus(Status status);

    String getUserId();

    void setUserId(String userId);
}
