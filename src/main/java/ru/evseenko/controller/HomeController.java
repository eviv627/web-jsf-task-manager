package ru.evseenko.controller;

import com.ocpsoft.pretty.faces.annotation.URLMapping;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Scope;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.context.WebApplicationContext;
import ru.evseenko.entity.dto.UserDTO;

import javax.faces.context.FacesContext;
import javax.inject.Named;

@Named("index")
@Scope(WebApplicationContext.SCOPE_REQUEST)
@URLMapping(id = "index",
        pattern = "/",
        viewId = "/WEB-INF/views/index.xhtml")
public class HomeController {

    @Getter
    private final String string;

    public HomeController() {
        final Object userObj = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (userObj instanceof String) {
            string = (String) userObj;
        } else if (userObj instanceof UserDTO) {
            string = ((UserDTO) userObj).getLogin();
        } else {
            string = "";
        }
    }
}
