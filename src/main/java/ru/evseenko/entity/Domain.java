package ru.evseenko.entity;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@MappedSuperclass
public abstract class Domain extends Identifiable {

    @Nullable
    @Column(name="name", nullable = false)
    private String name;

    @Nullable
    @Column(name="description", nullable = false)
    private String description;

    @Nullable
    @Column(name="start_date")
    private Date startDate;

    @Nullable
    @Column(name="end_date")
    private Date endDate;

    @Nullable
    @Column(name="create_date", nullable = false)
    private Date createDate;

    @Nullable
    @Column(name="status")
    @Enumerated(EnumType.STRING)
    private Status status;

    @Nullable
    @JoinColumn(name = "user_id", nullable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private User user;

}
