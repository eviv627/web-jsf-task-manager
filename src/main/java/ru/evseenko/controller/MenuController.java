package ru.evseenko.controller;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Scope;
import org.springframework.web.context.WebApplicationContext;
import ru.evseenko.util.DateFormatUtil;

import javax.inject.Named;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

@Named("menu")
@Scope(WebApplicationContext.SCOPE_REQUEST)
@NoArgsConstructor
public class MenuController {

    @NotNull
    private final SimpleDateFormat dateFormat = new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss", Locale.getDefault());

    public int getValue(final String str) {
        return Integer.valueOf(str);
    }

    public String dateFormat(final Date date) {
        dateFormat.setTimeZone(TimeZone.getTimeZone("Europe/Moscow"));
        return dateFormat.format(date);
    }
}
