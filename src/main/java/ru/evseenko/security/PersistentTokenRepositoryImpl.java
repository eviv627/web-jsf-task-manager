package ru.evseenko.security;

import org.jetbrains.annotations.NotNull;
import org.springframework.security.web.authentication.rememberme.PersistentRememberMeToken;
import org.springframework.security.web.authentication.rememberme.PersistentTokenRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.evseenko.entity.PersistentToken;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.Date;
import java.util.List;

@Transactional
@Repository("persistentTokenRepository")
public class PersistentTokenRepositoryImpl implements PersistentTokenRepository {

    private final EntityManager entityManager;

    public PersistentTokenRepositoryImpl(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public void createNewToken(final PersistentRememberMeToken token) {
        @NotNull final PersistentToken persistentToken = new PersistentToken();
        persistentToken.setUsername(token.getUsername());
        persistentToken.setSeries(token.getSeries());
        persistentToken.setToken(token.getTokenValue());
        persistentToken.setLastUsed(token.getDate());
        entityManager.persist(persistentToken);
    }

    @Override
    public void updateToken(String series, String tokenValue, Date lastUsed) {
        @NotNull final PersistentToken persistentToken =
                entityManager.find(PersistentToken.class, series);
        persistentToken.setToken(tokenValue);
        persistentToken.setLastUsed(lastUsed);
        entityManager.merge(persistentToken);
    }

    @Override
    public PersistentRememberMeToken getTokenForSeries(String series) {
        @NotNull final PersistentToken persistentToken = entityManager
                .find(PersistentToken.class, series);

        if (persistentToken != null) {
            return new PersistentRememberMeToken(
                    persistentToken.getUsername(),
                    persistentToken.getSeries(),
                    persistentToken.getToken(),
                    persistentToken.getLastUsed()
            );
        }
        return null;
    }

    @Override
    public void removeUserTokens(String username) {
        @NotNull final TypedQuery<PersistentToken> namedQuery = entityManager.createNamedQuery(
                PersistentToken.FIND_BY_USERNAME,
                PersistentToken.class
        );
        namedQuery.setParameter("username", username);
        @NotNull final List<PersistentToken> list = namedQuery.getResultList();
        if (list.size() > 0) {
            @NotNull final PersistentToken persistentToken = list.get(0);
            entityManager.remove(persistentToken);
        }
    }
}
