package ru.evseenko.entity.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import ru.evseenko.api.entity.Identifiable;
import ru.evseenko.entity.Role;

import javax.xml.bind.annotation.XmlSeeAlso;
import java.io.Serializable;
import java.util.*;

@Setter
@Getter
@XmlSeeAlso({Role.class})
public class UserDTO implements Serializable, UserDetails {

    public UserDTO() {
        id = "";
        login = "";
        passwordHash = "";
        password = "";
        passwordConfirm = "";
        role = Role.USER;
    }

    @NotNull
    private String id;

    @NotNull
    private String login;

    @NotNull
    private String passwordHash;

    @NotNull
    private String password;

    @NotNull
    private String passwordConfirm;

    @NotNull
    private Role role;

    private boolean active;

    @Override
    public String toString() {
        return "UserDTO{" +
                "id='" + id + '\'' +
                ", login='" + login + '\'' +
                ", passwordHash='" + passwordHash + '\'' +
                ", role=" + role +
                '}';
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return Collections.singletonList(role);
    }

    @Override
    public String getUsername() {
        return login;
    }

    @Override
    public boolean isAccountNonExpired() {
        return active;
    }

    @Override
    public boolean isAccountNonLocked() {
        return active;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return active;
    }

    @Override
    public boolean isEnabled() {
        return active;
    }
}
