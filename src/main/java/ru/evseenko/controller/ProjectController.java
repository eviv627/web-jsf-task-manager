package ru.evseenko.controller;

import com.ocpsoft.pretty.faces.annotation.URLMapping;
import com.ocpsoft.pretty.faces.annotation.URLMappings;
import org.jetbrains.annotations.NotNull;
import org.primefaces.event.RowEditEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.web.context.WebApplicationContext;
import ru.evseenko.api.service.IProjectService;
import ru.evseenko.entity.Status;
import ru.evseenko.entity.dto.ProjectDTO;
import ru.evseenko.entity.dto.UserDTO;
import ru.evseenko.exception.ServiceException;
import ru.evseenko.util.ControllerUtil;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import java.util.*;

@Named("project")
@Scope(WebApplicationContext.SCOPE_REQUEST)
@URLMappings( mappings = {
        @URLMapping(id = "project",
                pattern = "/project",
                viewId = "/WEB-INF/views/project/list.xhtml"),
        @URLMapping(id = "projectAdd",
                pattern = "/project/add",
                viewId = "/WEB-INF/views/project/add.xhtml")
})
public class ProjectController {

    private final IProjectService projectService;
    private final UserDTO user;
    private final ProjectDTO project;
    private List<ProjectDTO> projects;

    @Autowired
    public ProjectController(IProjectService projectService) {
        this.projectService = projectService;
        this.user = ControllerUtil.getCurrentUser();
        this.project = new ProjectDTO();
    }

    @PostConstruct
    public void init() {
        projects = projectService.getAll(user.getId());
    }

    public String addProject() {
        final String userId = user.getId();
        project.setUserId(userId);
        project.setId(UUID.randomUUID().toString());
        project.setCreateDate(new Date());
        projectService.persist(userId, project);

        @NotNull final FacesMessage msg = new FacesMessage("Project", "Project add success");
        FacesContext.getCurrentInstance().addMessage(null, msg);

        return "list.xhtml?faces-redirect=true";
    }

    public void onRowEdit(RowEditEvent event) {
        @NotNull final Object obj = event.getObject();
        @NotNull final ProjectDTO projectUpd;
        if (obj instanceof ProjectDTO) {
            projectUpd = (ProjectDTO) obj;
        } else {
            throw new ServiceException("invalid object");
        }

        projectService.update(user.getId(), projectUpd);

        @NotNull final FacesMessage msg = new FacesMessage("Project Edited", projectUpd.getName());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public void onRowCancel(RowEditEvent event) {
        final FacesMessage msg = new FacesMessage("Edit Cancelled", ((ProjectDTO) event.getObject()).getName());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public String goToAdd() {
        return "add.xhtml?faces-redirect=true";
    }

    public void deleteProject(final ProjectDTO project) {
        projects.remove(project);
        projectService.delete(user.getId(), project);
        @NotNull final FacesMessage msg = new FacesMessage("Project Deleted", project.getName());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public List<ProjectDTO> getProjects() {
        return projects;
    }

    public List<Status> getStatuses() { return Arrays.asList(Status.values()); }

    public void setName(String name) {
        project.setName(name);
    }

    public String getName() {
        return project.getName();
    }

    public void setDescription(String description) {
        project.setDescription(description);
    }

    public String getDescription() {
        return project.getDescription();
    }

    public void setStartDate(Date startDate) {
        project.setStartDate(startDate);
    }

    public Date getStartDate() {
        return project.getStartDate();
    }

    public void setEndDate(Date endDate) {
        project.setEndDate(endDate);
    }

    public Date getEndDate() {
        return project.getEndDate();
    }

    public void setStatus(Status status) {
        project.setStatus(status);
    }

    public Status getStatus() {
        return project.getStatus();
    }
}
