package ru.evseenko.service;

import org.jetbrains.annotations.NotNull;
import org.springframework.transaction.annotation.Transactional;
import ru.evseenko.api.entity.DomainDTO;
import ru.evseenko.api.repository.IDomainRepository;
import ru.evseenko.api.service.IDomainService;
import ru.evseenko.entity.Domain;
import ru.evseenko.exception.ServiceException;
import ru.evseenko.util.DomainEntityMapperUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public abstract class AbstractDomainService<T extends DomainDTO, D extends Domain> implements IDomainService<T> {

    @NotNull
    abstract IDomainRepository<D> getDomainRepository();

    abstract void updateEntity(D entity, T entityDTO);

    abstract void updateDTO(D entity, T entityDTO);

    @NotNull
    abstract Class<D> getClassEntity();

    @NotNull
    abstract Class<T> getClassDTO();

    @NotNull
    @Override
    @Transactional
    public T get(
            @NotNull final String userId,
            @NotNull final String id
    ) {
        @NotNull final T entityDTO = getInstanceDTO();
        @NotNull final Optional<D> optional = getDomainRepository().findById(id);
        if(!optional.isPresent()) {
            throw new ServiceException("No such entity");
        }
        @NotNull final D entity = optional.get();
        DomainEntityMapperUtil.setUpDomainDTO(entity, entityDTO);
        updateDTO(entity, entityDTO);

        return entityDTO;
    }

    @Override
    @Transactional
    public @NotNull List<T> getAll(@NotNull final String userId) {
        @NotNull final List<D> entities = getDomainRepository().findAll(userId);
        @NotNull final List<T> entitiesDTO = getListDTO(entities);

        return entitiesDTO;
    }

    @Override
    @Transactional
    public @NotNull List<T> findByNamePart(
            @NotNull final String userId,
            @NotNull final String name
    ) {
        @NotNull final List<D> entities = getDomainRepository().findByNamePart(userId, name);
        @NotNull final List<T> entitiesDTO = getListDTO(entities);

        return entitiesDTO;
    }

    @Override
    @Transactional
    public @NotNull List<T> findByDescriptionPart(
            @NotNull final String userId,
            @NotNull final String description
    ) {
        @NotNull final List<D> entities = getDomainRepository().findByDescriptionPart(userId, description);
        @NotNull final List<T> entitiesDTO = getListDTO(entities);

        return entitiesDTO;
    }

    @Override
    @Transactional
    public void update(
            @NotNull final String userId,
            @NotNull final T entityDTO
    ) {
        @NotNull final IDomainRepository<D> repository = getDomainRepository();
        @NotNull final Optional<D> optional = repository.findById(entityDTO.getId());
        if(!optional.isPresent()) {
            throw new ServiceException("No such entity");
        }
        @NotNull final D entity = optional.get();
        DomainEntityMapperUtil.setUpDomainEntity(entity, entityDTO);
        updateEntity(entity, entityDTO);

        if (!userId.equals(entityDTO.getUserId())) {
            throw new ServiceException("Illegal access");
        }
        repository.save(entity);
    }

    @Override
    @Transactional
    public void persist(
            @NotNull final String userId,
            @NotNull final T entityDTO
    ) {
        if (entityDTO.getId() == null) {
            entityDTO.setId(UUID.randomUUID().toString());
        }

        @NotNull final D entity = getInstanceEntity();
        DomainEntityMapperUtil.setUpDomainEntity(entity, entityDTO);
        updateEntity(entity, entityDTO);

        if (!userId.equals(entityDTO.getUserId())) {
            throw new ServiceException("Illegal access");
        }
        getDomainRepository().save(entity);
    }

    @Override
    @Transactional
    public void delete(
            @NotNull final String userId,
            @NotNull final T entityDTO
    ) {
        @NotNull final IDomainRepository<D> repository = getDomainRepository();
        @NotNull final Optional<D> optional = repository.findById(entityDTO.getId());
        if(!optional.isPresent()) {
            throw new ServiceException("No such entity");
        }
        @NotNull final D entity = optional.get();
        if (!userId.equals(entityDTO.getUserId())) {
            throw new ServiceException("Illegal access");
        }
        repository.delete(entity);
    }

    @Override
    @Transactional
    public void deleteAll(@NotNull final String userId) {
        @NotNull final List<D> list = getDomainRepository().findAll(userId);
        for (D entity : list) {
            getDomainRepository().delete(entity);
        }
    }

    @NotNull
    private T getInstanceDTO() {
        @NotNull final T entityDTO;
        try {
            entityDTO = getClassDTO().newInstance();
        } catch (Exception e) {
            throw new ServiceException(e.getMessage());
        }
        return  entityDTO;
    }

    @NotNull
    private D getInstanceEntity() {
        @NotNull final D entity;
        try {
            entity = getClassEntity().newInstance();
        } catch (Exception e) {
            throw new ServiceException(e.getMessage());
        }
        return entity;
    }

    @NotNull
    List<T> getListDTO(@NotNull final List<D> entities) {
        @NotNull final List<T> entitiesDTO = new ArrayList<>();

        for (@NotNull final D entity : entities) {
            @NotNull final T entityDTO = getInstanceDTO();
            DomainEntityMapperUtil.setUpDomainDTO(entity, entityDTO);
            updateDTO(entity, entityDTO);
            entitiesDTO.add(entityDTO);
        }

        return entitiesDTO;
    }
}
